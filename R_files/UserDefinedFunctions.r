
name_pets<-function(person,cats,dogs){

    result<-c(person,cats,dogs)
    return(result)             #the function returns the last object it sees.  
                               #Adding a return() specifies what to return to
                                #the main program
                               #But is not strictly necessary here. Try the
                                #code without it.  
}

name<-"Andrew"
number_of_cats<-4
number_of_dogs<-1

details<-name_pets(name, number_of_cats,number_of_dogs)  #this is the function
                                    #call. The arguments passed to the function 
                                    #do not have to have the same names as in
                                    #the source code.

details           #The function output is contained within the object 'details' 
                  #until a command is given to display it.
