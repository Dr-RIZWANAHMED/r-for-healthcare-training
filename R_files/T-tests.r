
data <- data.frame(ID=seq(1,25,1),
                   ScoreOne=rpois(25,20),
                   ScoreTwo=rpois(25,35))
t.test(data$ScoreOne,data$ScoreTwo)

boxplot(data$ScoreOne,data$ScoreTwo)

dataP <- data.frame(ID=seq(1,25,1),
                    Age=sample(18:99,25,replace=TRUE),
                    Gender=sample(1:2,25,replace=TRUE),
                    ScoreOne=sample(0:50,25,replace=TRUE),
                    ScoreTwo=sample(0:50,25,replace=TRUE))
t.test(dataP$ScoreOne,dataP$ScoreTwo,paired=TRUE)

boxplot(dataP$ScoreOne,dataP$ScoreTwo)
