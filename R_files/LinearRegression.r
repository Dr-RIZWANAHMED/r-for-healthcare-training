
distance <- sort(runif(100,1,70))
time <- sort(runif(100,5,120))
data <- cbind(distance,time)
data <- as.data.frame(data)
cor(data,use="all.obs",method="pearson")
a <- lm(distance ~ time, data=data)
summary(a)
b <- a$coefficients
b
plot(data$time,data$distance)
abline(a)

c <- summary(data$time)
cMin <- min(data$time)
cMax <- max(data$time)
d <- seq(cMin,cMax,by=0.05)
confInt <- predict(a, newdata=data.frame(time=d),
                   interval="confidence", level=0.95)
plot(data$time,data$distance, main="Distance over time",
     xlab="Time", ylab="Distance", pch=20)
abline(a)
lines(d,confInt[,2], col="blue", lty=2)
lines(d,confInt[,3], col="blue", lty=2)
