
x <- seq(1, 10, by=0.5)
x[2]

x[c(4,7)]

y <- c(3,7,5,2,8)
l <- y > 3
l
y[l]

y[-c(2,4)]

z <- matrix(c(1,2,5,3,2,4,6,4,5,3,6,1), nrow=3,
            ncol=4, byrow=TRUE)
print(z)

z[2,]
z[,4]
z[2,4]
z[c(1,2),]
z[c(T,F,T),]

data <- as.data.frame(z)
data

dataOne <- data$V1
dataOne

attach(data)
dataOne <- V1
dataOne
detach(data)

dataOne <- data[c("V2","V3")]
dataOne

dataTwo <- data[1:2,]
dataTwo

dataTwo <- subset(data, V1 >= 2)
dataTwo
dataThree <- subset(data, V1 >= 2, select=c(V2,V3))
dataThree

dataFour <- data[ which(data$V1 == 2 | data$V3 == 6),]
dataFour
